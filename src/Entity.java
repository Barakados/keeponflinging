import java.awt.Color;

import org.lwjgl.opengl.GL11;


public class Entity {

	float x,y,width,height;
	float vx,vy;
	Color color;
	final float maxSpeed=4.0f;
	public Entity(float x, float y, float width, float height){
		
		this.x = x+width/2;
		this.y = y+height/2;
		this.width = width;
		this.height = height;
		this.color = Color.BLACK;
		
	}
	public void move(){
		vx=Math.min(Math.max(-maxSpeed,vx), maxSpeed);
		vy=Math.min(Math.max(-maxSpeed,vy), maxSpeed);
		vx*=0.9;
		vy*=0.9;
		x+=vx;
		y+=vy;
		
	}
	public boolean collide(Entity e){
		float centerX = Math.abs(e.x - x);
		float centerY = Math.abs(e.y - y);
		float combinedWidth = (e.width + width)/2;
		float combinedHeight = (e.height + height)/2;
		if (centerX <= combinedWidth && centerY <= combinedHeight) {
			float offsetX = Math.abs(centerX - combinedWidth);
			float offsetY = Math.abs(centerY - combinedHeight);
			
			if (offsetX < offsetY) {
				if (x > e.x) {
					e.x -= offsetX;
				} else {
					e.x += offsetX;
				}
			} else {
				if (y > e.y) {
					e.y -= offsetY;
				} else {
					e.y += offsetY;
				}
			}
			return true;
		}else{
			return false;
		}
	}
	
	public Entity(float x, float y, float width, float height, Color color){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.color = color;
	}
	
	public void drawEntity(){
		
		GL11.glColor3f(color.getRed()/255, color.getGreen()/255, color.getBlue()/255);
		
		GL11.glBegin(GL11.GL_QUADS);
			GL11.glVertex2f(x - width/2, y - height/2);
			GL11.glVertex2f(x + width/2, y - height/2);
			GL11.glVertex2f(x + width/2, y + height/2);
			GL11.glVertex2f(x - width/2, y + height/2);
		GL11.glEnd();
		
	}
	
}
