import java.awt.Color;


public class Block extends Entity{

	
	
	public Block(float x, float y, float width, float height){
		super(x, y, width, height);
		color = Color.red;
	}
	
	public Block(float x, float y, float width, float height, Color color){
		super(x, y, width, height, color);
	}
	
	
	
	
}
