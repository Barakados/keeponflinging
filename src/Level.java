import java.util.ArrayList;
import java.util.Scanner;

public class Level {
	public ArrayList<Block> blocks = new ArrayList<Block>();
	public Player player;
	public Level(String path) {
		Scanner sc = new Scanner(this.getClass().getResourceAsStream(path));
		player = new Player(sc.nextFloat(),sc.nextFloat());
		int i = sc.nextInt();
		while (i>0){
			blocks.add(new Block(sc.nextFloat(),sc.nextFloat(),sc.nextFloat(),sc.nextFloat()));
			i--;
		}
	}
}
