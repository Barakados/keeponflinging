import java.awt.Color;

public class Player extends Entity {

	public float fuel=2.4f;

	public void move() {
		fuel = Math.min(2.4f,fuel);
		vy+=0.5;
		vx = Math.min(Math.max(-maxSpeed, vx), maxSpeed);
		vy = Math.min(Math.max(-maxSpeed, vy), maxSpeed);
		vx *= 0.9;
		x += vx;
		y += vy;
	}

	public Player(float x, float y) {
		super(x, y, 16, 32, Color.GREEN);

	}

}
