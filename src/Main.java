import java.awt.Font;
import java.util.ArrayList;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

import sun.font.TrueTypeFont;

public class Main {
	public ArrayList<Level> levels = new ArrayList<Level>();
	public Level selected;
	public Player player;
	public boolean[] keys = new boolean[4];
	public TrueTypeFont font;

	public Main() {
		selected = new Level("level1.txt");
		player = selected.player;
	}

	public void start() {

		try {
			Display.setDisplayMode(new DisplayMode(800, 600));
			Display.create();
		} catch (LWJGLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(0);
		}
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, 800, 600, 0, -1, 1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		while (!Display.isCloseRequested()) {
			input();
			update();
			Display.update();
		}
		
		Display.destroy();
	}

	private void update() {
		if (keys[0]) {
			if (player.fuel > 0) {
				player.vy -= 8;
				player.fuel-=0.1;
			}
		}
		if (keys[1]) {
			player.vx -= 0.3;
		}
		if (keys[2]) {
			player.vy += 0.1;
		}
		if (keys[3]) {
			player.vx += 0.3;
		}
		player.move();
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		for (Block b : selected.blocks) {
			b.collide(player);
			b.drawEntity();
		}
		player.drawEntity();

	}

	private void input() {
		if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
			keys[0] = true;
		} else {
			keys[0] = false;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
			keys[1] = true;
		} else {
			keys[1] = false;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
			keys[2] = true;
		} else {
			keys[2] = false;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
			keys[3] = true;
		} else {
			keys[3] = false;
		}
	}

	// http://lwjgl.org/wiki/index.php?title=Slick-Util_Library_-_Part_3_-_TrueType_Fonts_for_LWJGL
	// public void startscreen(){
	//
	// try{ Thread.sleep(1000); } catch (Exception e){
	// e.printStackTrace();
	// }
	// System.out.println("click to start!");
	// try{ Thread.sleep(1000); } catch (Exception e){
	// e.printStackTrace();
	// }
	// System.out.println("clear");
	// }

	public static void main(String[] args) {
		Main m = new Main();
		m.start();
		/*
		 * while(!Mouse.isButtonDown(0)){ m.startscreen(); }
		 */
	}
}
